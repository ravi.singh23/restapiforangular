const config=require('./config');
const {Strategy:JwtStrategy,ExtractJwt}=require('passport-jwt');
const {tokenTypes}=require('./tokens');
const {User}=require('../models');

const jwtOptions={
    jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey:config.jwt.secret
}
const jwtVerify=async (payload,done)=>{
    try {
        // console.log("jwt verify run");
        // console.log(payload);
        if (payload.type!=tokenTypes.ACCESS){
            throw new Error('Invalid access type');
        }
        const user=await User.findById(payload.sub);
        if (!user){
            console.log('JWT: User not found')
            return done(null,false);
        }
        // console.log('SENDING:   User found');
        done(null,user);
    }catch (err){
        console.log("error jwt verify")
        done(err,false);
    }
}
//init our jwt
const jwtStrategy=new JwtStrategy(jwtOptions,jwtVerify);

module.exports= {jwtStrategy};