const express = require('express');
const authController=require('../../controllers/auth.controller')
const validate=require('../../middlewares/validate');
const authValidation=require('../../validations/auth.validation');
const auth=require('../../middlewares/auth')
const router=express.Router();

router.post('/register',validate(authValidation.register),authController.signupUser);
router.post('/login',validate(authValidation.login),authController.loginUser);
router.post('/logout',validate(authValidation.logout),authController.loginUser);
router.post('/refresh-token',validate(authValidation.refreshToken),authController.refreshToken);
router.post('/forgot-password',validate(authValidation.forgotPassword),authController.forgotPassword);
router.post('/reset-password',validate(authValidation.resetPassword),authController.resetPassword);
router.post('/send-verification-email',auth(),authController.sendEmailverificationLink);
router.post('/verify-email',validate(authValidation.verifyEmail),authController.verifyEmail);

module.exports=router;



/*      ROUTE PAYLOAD REQUIRED FOR REQUEST TO FULFILL
* const register={
    body:({
        email:joi.string().required().email(),
        password:joi.string().required().custom(password),
        name:joi.string().required()
    })
}
const login={
    body: ({
        email:joi.string().required().email(),
        password:joi.string().required().custom(password)
    })
}
const resetPassword={
    query:({
        token:joi.string().required()
    }),
    body:({
        password:joi.string().required().custom(password)
    })
}
const logout={
    body:({
        refreshToken:joi.string().required()
    })
}
const refreshToken={
    body:({
        refreshToken:joi.string().required()
    })
}
const forgotPassword={
    body:({
        email:joi.string().email().required()
    })
}
const verifyEmail={
    body:({
        token:joi.string().required()
    })
}
 */