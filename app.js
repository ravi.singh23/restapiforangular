const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoSanatize=require("express-mongo-sanitize");
const xss=require('xss-clean');
const helmet=require("helmet");
const compression=require("compression");
const cors=require("cors");
const passport=require('passport')
const {jwtStrategy}=require('./config/passport')
const { errorConverter, errorHandler } = require('./middlewares/error');
const ApiError = require('./utils/ApiError');
const httpStatus=require('http-status')
const v1Routes=require("./routes/v1/index")

const app = express();

//set security HTTP headers
app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//sanatize request data
app.use(mongoSanatize());
app.use(xss());

//gzip compression
app.use(compression());

//enable cors
app.use(cors());
app.options("*",cors());

//jwt authentication
app.use(passport.initialize());
passport.use('jwt',jwtStrategy)
//v1 routes register
app.use('/v1',v1Routes);

// ERROR HANDLING
// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
