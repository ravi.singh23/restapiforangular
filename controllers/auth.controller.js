const httpStatus = require('http-status');
const catchAsync=require('../utils/catchAsync')
const {userService,authService,tokenService,emailService}=require('../services')

const loginUser=catchAsync(async (req,res)=>{
    console.log("Login user called");
    const {email,password}=req.body;
    const user=await authService.loginUserWithEmailAndPassword(email,password);
    console.log(user)
    const token=await tokenService.generateAuthToken(user);
    res.json({
        user,
        token
    })
})

//singing up the user, and generating the access tokens
const signupUser=catchAsync(async (req,res)=> {
    const user=await userService.createUser(req.body);
    const tokens=await tokenService.generateAuthToken(user);
    res.status(httpStatus.CREATED).json({
        info: "SignUp called",
        user,
        tokens
    })
})

//getting the access token by using the refresh token
const refreshToken=catchAsync(async (req,res)=>{
    const token=await authService.refreshAuth(req.body.refreshToken);
    res.send(...token);
})

//forgot password, generate the password reset token and  mail to the user
const forgotPassword=catchAsync(async (req,res)=>{
    console.log("Forgot password called")
    const resetPasswordToken=await tokenService.generatePasswordResetToken(req.body.email);
    await emailService.sendResetPasswordMail(req.body.email,resetPasswordToken);
    res.status(httpStatus.CREATED).json({
        resetPasswordToken:resetPasswordToken,
        message:'Password reset link sent to email'
    })
})

//reset password by checking the reset password token,
const resetPassword=catchAsync(async (req,res)=>{
    await authService.resetPassword(req.query.token,req.body.password);
    res.status(httpStatus.OK).send("password reset successfull");
})

//sending the email verification link to the user mail
const sendEmailverificationLink=catchAsync(async (req,res)=>{
    const verifyEmailToken=await tokenService.generateVerifyEmailToken(req.user);
    await emailService.sendEmailVerificationLink(req.user.email,verifyEmailToken);
    res.status(httpStatus.NO_CONTENT).json({
        emailVerifyToken:verifyEmailToken,
        messgae:'Sent email verification link to the user'
    });
})

//verify user email
const verifyEmail=catchAsync(async (req,res)=>{
    await authService.verifyEmail(req.query.token);
    res.status(httpStatus.NO_CONTENT).send('Email verified')
})

module.exports={
    loginUser,
    signupUser,
    verifyEmail,
    sendEmailverificationLink,
    resetPassword,
    forgotPassword,
    refreshToken
}