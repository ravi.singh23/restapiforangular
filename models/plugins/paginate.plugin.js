//function to paginate all the user data
//  sort by, limit, page, 3 options required
const paginate=(schema)=>{
    schema.statics.paginate=async function(filter,options){
        console.log('paginate start')
        console.log(options);
        let sort='';
        if (options.sortBy){    //sort by is a string
            const sortCriteria=[];
            options.sortBy.split(',').forEach(sortOpt=>{
                //each sort option is of the form, name:desc OR name:asc
                const [key,order]=sortOpt.split(':');
                sortCriteria.push((order === 'desc' ? '-' : '') + key);
            })
            console.log(sortCriteria)
            sort=sortCriteria.join(' ');
        }else {
            sort='createdAt';
        }
        console.log(sort)
        const limit=options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 10;
        const page=options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
        const skip=(page - 1) * limit;

        const countPromise=this.countDocuments(options).exec();
        //finding documents according to the filter and options
        let docsPromise=this.find(filter).sort(sort).skip(skip).limit(limit).exec();

        return Promise.all([countPromise,docsPromise]).then(values=>{
            const [totalResult,results]=values;
            const totalPages=Math.ceil(totalResult/limit);
            const result = {
                results,
                page,
                limit,
                totalPages,
                totalResult,
            };
            return Promise.resolve(result);
        })
    }
}

module.exports = paginate