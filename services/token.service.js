const jwt = require('jsonwebtoken');
const moment = require('moment');
const httpStatus = require('http-status');
const config=require('../config/config');
const {tokenTypes}=require('../config/tokens');
const userService=require('./user.service');
const {Token}=require('../models');
const apiError=require('../utils/ApiError');

//generate token: used to generate token of all tokenTypes
const generateToken=(userId,expires,type,secret=config.jwt.secret)=>{
    const payLoad={
        sub:userId,
        iat:moment().unix(),
        exp:expires.unix(),
        type
    };
    return jwt.sign(payLoad,secret);
}

//save a token to db: used to save a token, associated with a particular user and type in DB
const saveToken=async (token,userId,expires,type,blacklisted=false)=>{
    const tokenDoc=await Token.create({
        token,
        user:userId,
        expires:expires.toDate(),
        type,
        blacklisted
    });
    return tokenDoc;
}

//verifying a token: used to decode a token and find it in DB
const verifyToken=async (token,type)=>{
    const payload=jwt.verify(token,config.jwt.secret);
    const tokenDoc=Token.findOne({token,type,user:payload.sub,blacklisted:false});
    if (!tokenDoc){
        throw new Error("Token not found");
    }
    return tokenDoc;
}

//generate auth token and storing the refresh token in DB return the tokens
const generateAuthToken=async (user)=>{
    //creating the access token and its expiry
    const accessTokenExpires=moment().add(config.jwt.accessExpirationMinutes,'minutes');
    const accessToken=generateToken(user.id,accessTokenExpires,tokenTypes.ACCESS);

    //creating the refresh token and its expiry
    const refreshTokenExpires=moment().add(config.jwt.refreshExpirationDays,'days');
    const refreshToken=generateToken(user.id,refreshTokenExpires,tokenTypes.REFRESH);
    //saving the refresh token in the db
    await saveToken(refreshToken,user.id,refreshTokenExpires,tokenTypes.REFRESH);

    //returing the access token and refresh token to the client
    return {
        access:{
            token:accessToken,
            expires:accessTokenExpires
        },
        refresh:{
            token:refreshToken,
            expires:refreshTokenExpires
        }
    }
}

//generate password reset token
const generatePasswordResetToken=async (email)=>{
    const user=await userService.getUserByEmail(email);
    if (!user){
        throw new ApiError(httpStatus.NOT_FOUND,'No user found with this email');
    }
    const expires=moment().add(config.jwt.resetPasswordExpirationMinutes,'minutes');
    const resetPasswordToken=generateToken(user.id,expires,tokenTypes.RESET_PASSWORD);
    await saveToken(resetPasswordToken,user.id,expires,tokenTypes.RESET_PASSWORD);
    return resetPasswordToken;
}

//generate verify email token
const generateVerifyEmailToken=async (user)=>{
    const expires=moment().add(config.jwt.verifyEmailExpirationMinutes,'minutes');
    const verifyEmailToken=generateToken(user.id,expires,tokenTypes.VERIFY_EMAIL);
    await saveToken(verifyEmailToken,user.id,expires,tokenTypes.VERIFY_EMAIL);
    return verifyEmailToken;
}


module.exports={
    generateVerifyEmailToken,
    generatePasswordResetToken,
    generateToken,
    saveToken,
    generateAuthToken,
    verifyToken
}